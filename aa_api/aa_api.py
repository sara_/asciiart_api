import os
import aalib
from flask import request as REQUEST
from flask import Flask, jsonify
from flask_uploads import UploadSet, IMAGES, patch_request_class, configure_uploads
from PIL import Image, ImageOps

app = Flask(__name__)
app.config['UPLOADED_IMAGES_DEST'] = os.path.realpath(__file__)
images = UploadSet('images', IMAGES)
patch_request_class(app, 4 * 1024 * 1024)  # limit upload size to 4MB
configure_uploads(app, images)


class BadRequest(Exception):
    """Customizable error to raise on bad requests
    """

    status_code = 400

    def __init__(self, message, status_code=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        return dict(error=self.message)


@app.errorhandler(BadRequest)
def handle_badrequest(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


def img_to_ascii_art(img):
    # TODO this can be improved
    max_res = (128, 128)
    try:
        img = Image.open(img).convert('L')  # open & convert to greyscale
    except OSError:
        raise BadRequest("Unable to read uploaded image")

    img = ImageOps.invert(img)  # TODO good for light imgs, not so good for dark
    img.thumbnail(max_res)  # scale to max defined above, preserve aspect ratio

    w = int(img.size[0]/2)
    h = int(img.size[1]/2)
    # define a width & height for our ascii output based on resized image

    screen = aalib.AsciiScreen(width=w, height=h)
    screen.put_image((0, 0), img)
    art = screen.render()
    return art


def request_wants_plaintext(request):
    """
    Allow clients to explicitly request plaintext over default JSON response
    """
    # TODO in reality this is better handled by fully-featured content
    # negotiation utilities; this is just for quick & dirty smoke testing

    text_requested = 'text/plain' in request.accept_mimetypes
    json_requested = 'application/json' in request.accept_mimetypes
    # these are true when relevant accept headers are set, as well as in
    # the case of '*/*'. We want to send json in all instances except when
    # text/plain is explicitly specified (and application/json is not also
    # specified) OR when no accept mimetype is explicitly set.

    if text_requested and not json_requested:
        app.logger.debug("text/plain request, and not '*/*'")
        return True
    return False


@app.route('/upload', methods=['POST'])
def upload():
    request = REQUEST
    if 'file' not in request.files:
        raise BadRequest("No file upload provided")

    img = request.files['file']
    original_name = img.filename
    if images.file_allowed(img, original_name):
        art = img_to_ascii_art(img)
    else:
        app.logger.error("File with a non-image extension uploaded")
        raise BadRequest("Invalid file extension on uploaded file", 415)

    if request_wants_plaintext(request):
        return art
    else:
        return jsonify(original_filename=original_name, ascii_art=art)
