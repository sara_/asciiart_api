import os
try:
    from StringIO import StringIO as IO
except ImportError:
    from io import BytesIO as IO
from ..aa_api import img_to_ascii_art, app


class TestAsciiArtApi(object):
    def setup(self):
        app.testing = True
        self.app = app.test_client()

        cwd = os.path.dirname(__file__)
        self.test_img = os.path.join(cwd, 'butterfly.jpg')
        self.test_art = os.path.join(cwd, 'butterfly_to_ascii.txt')
        self.fakefile = "this is not a real image file".encode('utf-8')

    def test_img_to_ascii_art(self):
        """Verify output of ascii art conversion
        """

        with open(self.test_img, 'rb') as img:
            art = img_to_ascii_art(img)
            assert art is not None

        with open(self.test_art, 'rb') as txt:
            art = art.encode('utf-8')
            assert art == txt.read()

    def test_allowed_method(self):
        """We can only POST to /upload endpoint
        """

        req = self.app.get('/upload')
        assert req.status_code == 405

    def test_upload_file_is_required(self):
        """POSTs require an uploaded file
        """

        req = self.app.post('/upload', data={'file': 'nothing_here'})
        assert req.status_code == 400

    def test_bad_file_extension(self):
        """Uploaded files must have image-type file extensions
        """
        req = self.app.post(
            '/upload',
            content_type='multipart/form-data',
            data={
                'file': (IO(self.fakefile), 'badfiletype.txt')})
        assert req.status_code == 415
