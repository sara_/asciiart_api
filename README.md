# ASCII Art API

An API for converting images into ASCII art.

## Developer Setup

Requirements:

* flask
* flask-uploads
* python-aalib
* pillow
* Python 2.7 or 3.5+

A Python virtualenv is recommended for local development:

`$ virtualenv --python python3 env`

### Setting Up A Development Environment

From the project directory, activate the project's virtualenv:

`$ source env/bin/activate`

With the virtualenv active, install dependencies:

`$ pip install -r requirements.txt`

Set necessary environment variables:

`$ export FLASK_APP="aa_api/aa_api.py"`

`$ export FLASK_DEBUG=1`

Once that's complete, you can run the development server:

`$ flask run`

### Usage

With the local dev server running, you can access the API via normal means,
e.g., cURL or Postman.

The one available endpoint in this API is at `/upload`, and accepts
`multipart/form-data` POSTs. A form field called `file` must be included,
containing a single image file. Common image file types are accepted; e.g.,
.jpg, .png, .bmp.

The API returns a JSON response of the format:

```

{
    "original_filename": <original input's filename here>

    "ascii_art": <ASCII output here>
}

```

The JSON response includes line breaks (additional parsing would be required to
display these visually).

For quick visualization of ASCII output, you can request a plain text response
by setting the `Accept` header to `text/plain`.


### Examples

_Sample images are available in `aa_api/tests/`: images are
[CC0](https://creativecommons.org/publicdomain/zero/1.0/) licensed, and made
available by the [Wikimedia
Commons](https://commons.wikimedia.org/wiki/Main_Page)._

Using cURL - adjust local server address & port as needed:

1) JSON response (default)

`$ curl -X POST -F file=@aa_api/tests/butterfly.jpg 127.0.0.1:5000/upload`


2) plain text response

`$ curl -X POST -F file=@aa_api/tests/butterfly.jpg 127.0.0.1:5000/upload -H accept:text/plain`


### Testing

To run the tests, use pytest:

`$ pytest -v`


You can also use Tox to test against multiple Python versions. If Tox is not
already installed on your dev system, you can install it with:

`$ pip install --user tox`

(For more info on Tox, or help installing, [see here](<http://tox.readthedocs.org/en/latest/>))

With Tox installed, from within the project root do:

`$ tox`

# Notes

_This project is just a demo, and not intended for use in production._

### Third-Party Libraries

_This project uses the following free & open source libraries:_

- Flask
    -  Flask is great for a simple HTTP service like this one, where we didn't need
    anything more than basic routing, and didn't need a lot of boilerplate
    overhead. At the same time a Flask app is also easily scaled up in the future
    (e.g., while we didn't take advantage of it here, the option to add a database
    on the backend is available down the road). Flask is a mature framework with
    wide use in production apps, with its most recent release being within the last
    month.

- flask-uploads
    - This flask extension simplifies file handling in Flask apps. I used it
    because I realized I was about to start reinventing wheels it already has
    available (e.g., file extension whitelisting/blacklisting). The extension is
    suggested by the Flask docs themselves, and appears to be widely used.

- aalib & python-aalib
    - `aalib` is a C library for creating ASCII art that I was [already familiar
    with](https://www.youtube.com/watch?v=0nRPoS2WDJA). Although it is no
    longer actively developed, the library has been in use for decades: it is
    robust, extensively tested by years of wide use for the specific purpose of
    converting images to ASCII.

    - `python-aalib` is a thin ctypes wrapper around `aalib`: its
    Python bindings are the most direct way of getting access to `aalib` from a
    Flask app like this one.

- pillow
    - This project needed very little image manipulation: just enough (scaling,
    converting to greyscale, and inverting) to provide what `aalib` needed to do
    the real magic. `Pillow` is well-proven, widely-used, and has a long history of
    active development (its most recent release is from just over a month ago).
    Even `python-aalib`'s own example suggests its use, so it seemed reasonable to
    use it here.
