from setuptools import setup

setup(
    name="asciiart_api",
    version="0.0.0",
    classifiers=[
        "do not upload to pypi"
    ],
    install_requires=[
        'Flask==0.12.2',
        'Flask-Uploads==0.2.1',
        'Pillow==4.1.1',
        'pytest==3.1.2',
        'python-aalib==0.3.2',
    ],
)
